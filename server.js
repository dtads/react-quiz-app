/**
 * Created by Doyin on 30/01/2018.
 */

var bodyParser = require('body-parser')
const express = require('express');
const app = express();
//CORS middleware
const allowCrossDomain = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', 'example.com');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	
	next();
}

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
}));
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

//...
app.all('/*', function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

const port = process.env.PORT || 5000;

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/data/quiz', (req, res) => {
	res.send(QUIZ_DATA);
});

app.post('/data/result', (req, res) => {
	res.send(getResult(req.body));
});

app.listen(port, () => console.log(`Listening on port ${port}`));

const getResult = (response) => {
	//** calculate real score
	let score = Math.floor((Math.random() * 11) + 1);
	return {
		total: score
	};
}

const QUIZ_DATA = {
	questions: [
		{id: '001', title: '2 + 2', answers: ['233', '12', '4', '38'], selectedAnswers: []},
		{id: '002', title: '5 + 6', answers: ['11', '21', '45', '56'], selectedAnswers: []},
		{id: '003', title: '10 + 10', answers: ['4', '20', '34', '5'], selectedAnswers: []},
		{id: '004', title: '13 + 11', answers: ['6', '2', '24', '25'], selectedAnswers: []},
		{id: '005', title: '15 + 7', answers: ['3', '25', '94', '65'], selectedAnswers: []},
		{id: '006', title: '14 + 7', answers: ['2', '21', '24', '55'], selectedAnswers: []},
		// {id: '007', title: '20 + 9', answers: ['1', '29', '54', '51'], selectedAnswers: []},
		// {id: '008', title: '29 + 3', answers: ['32', '65', '14', '2'], selectedAnswers: []},
		// {id: '009', title: '13 + 17', answers: ['4', '30', '24', '5'], selectedAnswers: []},
	]
}
