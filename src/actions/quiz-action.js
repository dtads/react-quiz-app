/**
 * Created by Doyin on 30/01/2018.
 */

import axios from 'axios';
import { ADD_ANSWER_SELECTED } from '../constant/types';
import { REMOVE_ANSWER_SELECTED } from '../constant/types';
import { FETCH_QUIZ, QUIZ_SCORE } from '../constant/types';

export const addAnswerSelected = (question_id, answer_id) => ({
	type: ADD_ANSWER_SELECTED,
	payload: {question_id, answer_id}
})

export const removeAnswerSelected = (question_id, answer_id) => ({
	type: REMOVE_ANSWER_SELECTED,
	payload: {question_id, answer_id}
})

export const postQuiz = (quiz_data) => {
	return (dispatch) => {
		axios.post('http://localhost:5000/data/result',{quiz: quiz_data})
			.then((response)=>{
				dispatch({
					type : QUIZ_SCORE,
					payload: response.data
				})
			})
			.catch((err)=>{
				console.log(err ,'Error on page like ', err )
			});
	}
};

export const fetchQuiz = () => {
	return (dispatch) => {
		axios.get('http://localhost:5000/data/quiz')
			.then(response => {
				dispatch({
					type: FETCH_QUIZ,
					payload: response.data
				})
			})
			.catch((error)=> {
				console.log('Something went wrong ', error);
			});
	}
};
