/**
 * Created by Doyin on 29/01/2018.
 */
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import QuestionsReducer from '../reducers/quiz-reducer';


export default () => {
	const createStoreWithMiddlware = applyMiddleware(reduxThunk) (createStore);
	const store = createStoreWithMiddlware(QuestionsReducer);
	return store;
}
//
// export default ()=> {
// 	const store = createStore(QuestionsReducer);
// 	return store;
// }