/**
 * Created by Doyin on 29/01/2018.
 */
import {ADD_ANSWER_SELECTED} from '../constant/types';
import {REMOVE_ANSWER_SELECTED} from '../constant/types';
import {QUIZ_SCORE} from '../constant/types';
import {FETCH_QUIZ} from '../constant/types';

const INITIAL = {
	questions:[],
	total_score: ''
}

const QuizReducer = (state = INITIAL, action) => {

	switch (action.type) {
		case ADD_ANSWER_SELECTED: {
			return {
				...state,
				questions: state.questions.map((question)=>{
					if(question.id === action.payload.question_id){
						question.selectedAnswers = [...question.selectedAnswers, action.payload.answer_id]
					}
					return question
				})
			}
		}
		
		case REMOVE_ANSWER_SELECTED: {
			return {
				...state,
				questions: state.questions.map((question)=>{
					if(question.id === action.payload.question_id){
						question.selectedAnswers = question.selectedAnswers.filter((element) => element !== action.payload.answer_id);
					}
					return question
				})
			}
		}
		
		case FETCH_QUIZ:{
			const data = action.payload;
			return{...state, ...data}
		}
				
		case QUIZ_SCORE:{
			const data = action.payload.total;
			return{...state, total_score:data}
		}
		
		default:
			return state;
	}
}

export default QuizReducer;


