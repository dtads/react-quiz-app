import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// import 'bootstrap/dist/css/bootstrap.css';
import { connect } from 'react-redux';
import QuestionList from './components/question-list';
import QuizResultModal from './components/quiz-result-modal';
import QuizSubmitForm from './components/quiz-submit-form';
import {postQuiz, fetchQuiz} from './actions/quiz-action';


class App extends Component {
	
	componentDidMount(){
		this.props.dispatch(fetchQuiz());
	}
	state = {
		options: [],
		showResult: undefined
	}
	
	
	handleSubmitQuizForm = (evt)=> {
		evt.preventDefault();
		this.props.dispatch(postQuiz(this.props.questions));
		this.handleClearSelectedOption()
	};
	
	handleClearSelectedOption = ()=> {
		this.setState((prevState)=> ({
			showResult: !prevState.showResult
		}));
		//can posibly dispatch reset from here
	}
	
	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo"/>
					<h1 className="App-title">React Quick Quiz</h1>
				</header>
				<div className="container">
					<QuestionList/>
					
					<QuizSubmitForm
						questions={this.props.questions}
						onSubmit = {this.handleSubmitQuizForm}
					/>
					
					<QuizResultModal
						showResult={this.state.showResult}
						score={this.props.score}
						handleClearSelectedOption={this.handleClearSelectedOption}
					/>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		questions: state.questions,
		score: state.total_score
	}
}
export default connect(mapStateToProps)(App);
