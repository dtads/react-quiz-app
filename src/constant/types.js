/**
 * Created by Doyin on 30/01/2018.
 */
export const ADD_ANSWER_SELECTED = "ADD_ANSWER_SELECTED";
export const REMOVE_ANSWER_SELECTED = "REMOVE_ANSWER_SELECTED";
export const POST_QUIZ = "POST_QUIZ";
export const FETCH_QUIZ = "FETCH_QUIZ";
export const QUIZ_SCORE = "QUIZ_SCORE";
