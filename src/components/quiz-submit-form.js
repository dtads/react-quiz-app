/**
 * Created by Doyin on 30/01/2018.
 */
import React from 'react';
const QuizSubmitForm = (props) => {
	const isAllQuestionNotAnswered = props.questions.some((question)=> {
		if (question.selectedAnswers.length === 0) return true;
	})
	
	return (
		(isAllQuestionNotAnswered) ? ('') : (
			<form onSubmit={props.onSubmit} className="quiz-form">
				<button className="button-btn">Submit Quiz</button>
			</form>
		)
	)
}

export default QuizSubmitForm;