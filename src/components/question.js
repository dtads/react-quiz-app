/**
 * Created by Doyin on 29/01/2018.
 */
import React, { Component } from 'react';

const QuestionItem = (props) => {
	const isSelected = (id) => {
		//check if item is selected
		const selectedAnswer = props.question.selectedAnswers;
		return selectedAnswer.filter(selected => selected === id).length !== 0;
	}
	
	const renderAnswer = (answer, id) => {
		return (
			<li
				key={id}
				className="quiz-option">
				<label className="quiz-custom-label" htmlFor={id}>
					<input
						type="checkbox"
						id={props.question.id}
						value={id}
						defaultChecked={isSelected(id)}
						onChange={props.onAnswerSelected}
						className="quiz-checkbox"
					/>
					{answer}</label>
				<span className="checkmark"></span>
			</li>
		)
	}
	
	return (
		<div className="quiz-card">
			<div className="quiz-header">
				<h2>{props.index})  Question {props.question.title}</h2>
			</div>
			<ul className="quiz-options">
				{props.question.answers.map(renderAnswer)}</ul>
		</div>
	)
}

export default QuestionItem;