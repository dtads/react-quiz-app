/**
 * Created by Doyin on 29/01/2018.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import QuestionItem from './question';
import { addAnswerSelected, removeAnswerSelected } from '../actions/quiz-action'

class QuizQuestions extends Component {
	
	handleAnswerSelected = (evt) => {
		const dispatch = this.props.dispatch;
		const question_id = evt.target.id,
			answer_id = Number(evt.target.value);
		
		(evt.target.checked) ?
			dispatch(addAnswerSelected(question_id,answer_id)):
			dispatch(removeAnswerSelected(question_id,answer_id));
	}
	
	renderQuestions = (question, index)=> (
		
		<QuestionItem
			index={index+1}
			key={question.id}
			question={question}
			onAnswerSelected={this.handleAnswerSelected}
		/>
	)
	
	render() {
		return (
			<div >
				{
					(this.props.questions.length === 0) ?
						(<p>No item in the list </p>) :
						this.props.questions.map(this.renderQuestions )
				}
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
		questions: state.questions
	}
)
export default connect(mapStateToProps) (QuizQuestions);