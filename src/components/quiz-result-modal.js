/**
 * QuizResult Class
 */
import React, { Component } from 'react';
import ReactModal from 'react-modal';

const QuizResultModal = (props) => {
	
	return (
		<ReactModal
			isOpen={props.showResult}
			onRequestClose={props.handleClearSelectedOption}
			contentLabel="Minimal React Modal Example"
			closeTimeoutMS={200}
			className="modal"
		>
			<h3 className="modal__title">Quiz Result</h3>
			{props.showResult && <p className="modal__body">
				Your total score is {props.score}
			</p>}
			<button className="button-btn" onClick={props.handleClearSelectedOption}>Close.</button>
		</ReactModal>
	)
}
export default QuizResultModal;